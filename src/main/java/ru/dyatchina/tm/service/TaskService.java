package ru.dyatchina.tm.service;

import ru.dyatchina.tm.entity.Project;
import ru.dyatchina.tm.entity.Task;
import ru.dyatchina.tm.entity.User;
import ru.dyatchina.tm.enums.UserRole;
import ru.dyatchina.tm.repository.ProjectRepository;
import ru.dyatchina.tm.repository.TaskRepository;
import ru.dyatchina.tm.repository.UserRepository;

import java.util.Collections;
import java.util.List;

public class TaskService {

    private final TaskRepository taskRepository;
    private final ProjectRepository projectRepository;
    private final UserRepository userRepository;
    private final AuthService authService;

    public TaskService(TaskRepository taskRepository,
                       ProjectRepository projectRepository,
                       UserRepository userRepository, AuthService authService) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
        this.userRepository = userRepository;
        this.authService = authService;
    }

    public void create(String name, String description) {
        Task task = new Task();
        //if (name == null || name.isEmpty()) return null;
        //if (description == null || description.isEmpty()) return null;
        task.setName(name);
        task.setDescription(description);
        taskRepository.save(task);
        //return taskRepository.create(name,description);
    }

    public boolean addTaskToProject(Long projectId, Long taskId) {
        Project project = projectRepository.findById(projectId);
        Task task = taskRepository.findById(taskId);
        if(task != null && project != null) {
            task.setProjectId(projectId);
            return true;
        }
        return false;
    }

    public boolean deleteTaskFromProject( Long taskId) {
        Task task = taskRepository.findById(taskId);
        if(task != null) {
            task.setProjectId(null);
            return true;
        }
        return false;
    }

    public List<Task> getAllTasks() {
        User currentUser = authService.getCurrentUser();
        if (currentUser == null) {
            return Collections.emptyList();
        }
        if (currentUser.getRole() == UserRole.ADMIN) {
            return taskRepository.findAllOrderByName();
        } else {
            return taskRepository.findByUserIdOrderByName(currentUser.getId());
        }
    }

    public Task getTaskById(Long id) {
        return taskRepository.findById(id);
    }

    public Task getTaskByIndex(int index) {
        return taskRepository.findByIndex(index);
    }

    public List<Task> getTasksByName(String name) {
        return taskRepository.findByName(name);
    }

    public List<Task> getTasksByProjectId(Long projectId) {
        return taskRepository.findAddByProjectId(projectId);
    }

    public void deleteAllTasks() {
        taskRepository.deleteAll();
    }

    public Task deleteTaskById(Long id) {
        return taskRepository.deleteById(id);
    }

    public Task deleteTaskByIndex(int index) {
        return taskRepository.deleteByIndex(index);
    }

    public List<Task> deleteTaskByName(String name) {
        return taskRepository.deleteByName(name);
    }

    public boolean addTaskToUser(Long taskId, Long userId) {
        User user = userRepository.findById(userId);
        Task task = taskRepository.findById(taskId);
        User currentUser = authService.getCurrentUser();
        if (user == null || task == null || currentUser == null) {
            return false;
        }
        if (!currentUser.getId().equals(userId) || currentUser.getRole() != UserRole.ADMIN) {
            return false;
        }

        task.setUserId(userId);
        taskRepository.save(task);
        return true;
    }
}