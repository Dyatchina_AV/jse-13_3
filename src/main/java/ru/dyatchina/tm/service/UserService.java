package ru.dyatchina.tm.service;

import org.apache.commons.codec.digest.DigestUtils;
import ru.dyatchina.tm.entity.User;
import ru.dyatchina.tm.enums.UserRole;
import ru.dyatchina.tm.repository.UserRepository;

import java.util.List;


public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public boolean createUser(String login, String password, UserRole role) {
        if(userRepository.findByLogin(login) != null) {
            return false;
        }
        User user = new User();
        user.setLogin(login);
        user.setPassword(DigestUtils.md5Hex(password));
        user.setRole(role);
        userRepository.save(user);
        return true;
    }

    public void updateUser(User user) {
        userRepository.save(user);
    }

    public User getUserByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public void deleteAllUsers() {
        userRepository.deleteAll();
    }
}