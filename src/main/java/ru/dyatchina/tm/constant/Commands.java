package ru.dyatchina.tm.constant;

public class Commands {
    public static final String HELP = "help";
    public static final String VERSION = "version";
    public static final String ABOUT = "about";
    public static final String HISTORY = "history";
    public static final String EXIT = "exit";

    public static final String PROJECT_CREATE = "project-create";
    public static final String PROJECT_CLEAR = "project-clear";
    public static final String PROJECT_LIST = "project-list";
    public static final String PROJECT_FIND_BY_ID = "project-by-id";
    public static final String PROJECT_FIND_BY_INDEX = "project-by-index";
    public static final String PROJECT_FIND_BY_NAME = "project-by-name";
    public static final String PROJECT_DELETE_BY_ID = "project-delete-by-id";
    public static final String PROJECT_DELETE_BY_INDEX = "project-delete-by-index";
    public static final String PROJECT_DELETE_BY_NAME = "project-delete-by-name";

    public static final String TASK_CREATE = "task-create";
    public static final String TASK_CLEAR = "task-clear";
    public static final String TASK_LIST = "task-list";
    public static final String TASK_FIND_BY_ID = "task-by-id";
    public static final String TASK_FIND_BY_INDEX = "task-by-index";
    public static final String TASK_FIND_BY_NAME = "task-by-name";
    public static final String TASK_DELETE_BY_ID = "task-delete-by-id";
    public static final String TASK_DELETE_BY_INDEX = "task-delete-by-index";
    public static final String TASK_DELETE_BY_NAME = "task-delete-by-name";
    public static final String TASK_LIST_BY_PROJECT = "task-list-by-project";
    public static final String TASK_ADD_TO_PROJECT = "task-add-to-project";
    public static final String TASK_DELETE_FROM_PROJECT = "task-delete-from-project";

    public static final String USER_CREATE = "user-create";
    public static final String USER_LIST = "user-list";
    public static final String USER_CLEAR = "user-clear";

    public static final String LOGIN = "login";
    public static final String LOGOUT = "logout";
    public static final String CHANGE_PASSWORD = "change-password";
    public static final String CHANGE_USER_PASSWORD = "change-user-password";

}
