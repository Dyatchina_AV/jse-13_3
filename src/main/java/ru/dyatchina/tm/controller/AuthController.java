package ru.dyatchina.tm.controller;

import ru.dyatchina.tm.service.AuthService;

import static ru.dyatchina.tm.constant.Commands.*;

public class AuthController extends AbstractController {
    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @Override
    public void printAcceptingCommands() {
        System.out.println(LOGIN + " <login> <password> do login");
        System.out.println(LOGOUT + " do logout");
        System.out.println(CHANGE_PASSWORD + " <current password> <new password> change current user password");
        System.out.println(CHANGE_USER_PASSWORD + " <login> <new password> change user password (ADMIN Role required");
    }

    @Override
    public boolean execute(String[] commandWithArgs) {
        switch (commandWithArgs[0]) {
            case LOGIN:
                doLogin(commandWithArgs);
                return true;
            case LOGOUT:
                authService.logout();
                System.out.println("Logout success");
                return true;
            case CHANGE_PASSWORD:
                changePassword(commandWithArgs);
                return true;
            case CHANGE_USER_PASSWORD:
                changeUserPassword(commandWithArgs);
                return true;
            default:
                return false;
        }
    }

    private void changeUserPassword(String[] commandWithArgs) {
        if (commandWithArgs.length < 3) {
            System.out.println();
            return;
        }
        String login = commandWithArgs[1];
        String newPassword = commandWithArgs[2];
        boolean result = authService.changeUserPassword(login, newPassword);
        if (result) {
            System.out.println("Password changed");
        } else {
            System.out.println("Password not changed");
        }
    }

    private void changePassword(String[] commandWithArgs) {
        if (commandWithArgs.length < 3) {
            System.out.println();
            return;
        }
        String currentPassword = commandWithArgs[1];
        String newPassword = commandWithArgs[2];
        boolean result = authService.changeCurrentUserPassword(currentPassword, newPassword);
        if (result) {
            System.out.println("Password changed");
        } else {
            System.out.println("Password not changed");
        }
    }

    private void doLogin(String[] commandWithArgs) {
        if (commandWithArgs.length < 3) {
            System.out.println();
            return;
        }
        String login = commandWithArgs[1];
        String password = commandWithArgs[2];

        boolean result = authService.login(login, password);
        if (result) {
            System.out.println("Login success");
        } else {
            System.out.println("Login failed");
        }
    }
}

